import java.util.Arrays;

public class Notepad {

    protected int _count = 0;

    protected String[] _lines;

    public int getSize()
    {
        return _lines.length;
    }

    public int getCount()
    {
        return _count;
    }

    public Notepad()
    {
        _lines = new String[10];
    }

    public Notepad(int capacity)
    {
        if(capacity > 0)
        {
            _lines = new String[capacity];
        }
        else
        {
            _lines = new String[10];
        }
    }

    public void Add(String line)
    {
        if(getCount() >= getSize())
        {
            _lines = Arrays.copyOf(_lines, getSize()*2);
        }

        _lines[getCount()] = line;
        _count++;
    }

    public void Delete()
    {
        if(0 == getCount())
        {
            return;
        }

        _lines[getCount()] = null;
        _count--;

        if(getSize() >= getCount() * 2 + 1)
        {
            _lines = Arrays.copyOf(_lines, getSize()/2);
        }
    }

    public void Print()
    {
        for(int i = 0; i < getCount(); i++)
        {
            System.out.println(_lines[i]);
        }
    }
}
