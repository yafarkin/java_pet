
public class Main
{
    public static long fibb(long startNum)
    {
        if(startNum < 2)
        {
            return startNum;
        }

        return fibb(startNum-1) + fibb(startNum-2);
    }

    public static long fact(long startNum)
    {
        long result = 1;
        for(long cnt = 0; cnt < startNum; cnt++)
        {
            result = result * (cnt + 1);
        }
        return result;
    }

    public static void main(String[] args)
    {
        // task 1. fibbonachi
        for(int idx = 0; idx < 10; idx++)
        {
            System.out.println(idx + " = " + fibb(idx));
        }

        // task 2. factorial
        long num = 6;
        System.out.println("factorial of " + num + " = " + fact(num));


        // task 3. notepad
        Notepad notepad = new Notepad();
        notepad.Add("line1");
        notepad.Add("line2");
        notepad.Add("line3");
        notepad.Add("line4");
        notepad.Add("line5");
        notepad.Add("line6");
        notepad.Add("line7");
        notepad.Add("line8");
        notepad.Add("line9");
        notepad.Add("line10");
        notepad.Add("line11");
        notepad.Delete();
        notepad.Print();
        System.out.println("count: " + notepad.getCount());
        System.out.println("capacity: " + notepad.getSize());

        notepad.Delete();
        notepad.Delete();
        notepad.Delete();
        notepad.Delete();
        notepad.Delete();
        notepad.Delete();
        notepad.Print();
        System.out.println("count: " + notepad.getCount());
        System.out.println("capacity: " + notepad.getSize());

    }
}
